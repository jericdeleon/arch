# Bootstrap - brew

## Station

1. Install Homebrew
2. Bootstrap host
    1. Install Ansible
    2. Download [bootstrap-host](https://gitlab.com/jericdeleon/bootstrap-host)
    3. Verify `playbook` values
    4. Verify `group_vars/all.yml` values
    5. Run playbook: `./bootstrap.sh`
3. If shell is not set, reboot
4. Edit System Preferences for personal preferences
    1. Change Caps Lock to Control
5. Install [dotfiles](https://gitlab.com/jericdeleon/dotfiles/-/blob/master/dot_install_dotfiles.md)
6. Install runtimes in `asdf` for `vim`
    - `nodejs`
        - `eslint`
        - `import-sort-cli`
        - `prettier`
        - `typescript`
        - `vls`
    - `ruby`
        - `rubocop`
        - `solargraph`
7. Install repos
    - [dockerfiles](https://gitlab.com/jericdeleon/dockerfiles)
    - [notes](https://gitlab.com/jericdeleon/notes)
    - [ledger](https://gitlab.com/jericdeleon/ledger)
    - [password-store](https://gitlab.com/jericdeleon/password-store)
8. Update at will / on distro upgrade
    1. Rerun bootstrap instructions (should be idempotent)
    2. Update dotfiles (bootstrap will overwrite some files)
