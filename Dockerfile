FROM ubuntu:latest

# Setup ssh server
RUN DEBIAN_FRONTEND=noninteractive apt update
RUN DEBIAN_FRONTEND=noninteractive apt install -y sudo openssh-server
RUN mkdir /var/run/sshd
RUN sed -i 's/^\#PermitRootLogin.*/PermitRootLogin yes/' /etc/ssh/sshd_config
EXPOSE 22

# Setup root user
RUN echo 'root:root' | chpasswd

# Setup vagrant user
RUN adduser vagrant
RUN echo 'vagrant:vagrant' | chpasswd
RUN usermod -aG sudo vagrant
RUN echo 'vagrant ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Start sshd manually
CMD ["/usr/sbin/sshd", "-D"]
