# Bootstrap - SUSE
SUSE installation guide, for the ff. targets:
  - Vagrant (`opensuse/Tumbleweed.x86_64`)
  - Spins:
    - Tumbleweed is for desktops / laptops, rolling release
    - Leap is for desktops / laptops, point release
    - JeOS is for traditional datacenters (Hyper-V, VMware, KVM, Xen)
    - MicroOS is for container hosts and edge devices
      - transactional updates make MicroOS immutable
      - can be used locally through MicroOS Desktop
    - Kubic is a MicroOS variant, specialized for Kubernetes

# Installation
## Vagrant
1. Install Vagrant
    - use `vagrant-libvirt` plugin
2. Boostrap host
    1. Verify `Vagrantfile` values
    2. Verify `playbook` values
    3. Verify `group_vars/all.yml` values
    4. Provision machine: `vagrant up`
3. Install [dotfiles](https://gitlab.com/jericdeleon/dotfiles/-/blob/master/dot_install_dotfiles.md)

## Station
1. Flash bootable USB
2. Run installer
    1. Repositories Initialization
        - Main Update Repository
        - Main Repository (OSS)
        - Main Repository (NON-OSS)
    2. Suggested Partitioning
        1. Guided Setup
            - Enable LVM
            - Enable Disk Encryption
        2. Manual Setup

            | Function / Name | Type      | Role                     | Format | Format Options                                             | Partition ID         | Encrypted | Mount     |
            |-----------------|-----------|--------------------------|--------|------------------------------------------------------------|----------------------|-----------|-----------|
            | Function: esp   | Partition | Role: EFI Boot Partition | FAT32  | FAT Size: 32, Number of FATs: auto, Root Dir Entries: auto | EFI System Partition | No        | /boot/efi |
            | Function: pv    | Partition | Role: Raw Volume         | No     |                                                            | Linux LVM            | Yes       | No        |
            | Name: system    | VG        |                          |        | Physical Extent Size: 4MiB                                 |                      |           |           |
            | Name: root      | LV        | Role: Operating System   | Btrfs  |                                                            |                      | No        | /         |
            | Name: swap      | LV        | Role: Swap               | Swap   |                                                            |                      | No        | swap      |

3. Bootstrap host
    1. Install Ansible
    2. Download [bootstrap-host](https://gitlab.com/jericdeleon/bootstrap-host)
    3. Verify `playbook` values
    4. Verify `group_vars/all.yml` values
    5. Run playbook: `./bootstrap.sh`
4. If shell is not set, reboot
5. Install [dotfiles](https://gitlab.com/jericdeleon/dotfiles/-/blob/master/dot_install_dotfiles.md)
6. Install runtimes in `asdf` for `vim`
    - `nodejs`
        - `eslint`
        - `import-sort-cli`
        - `prettier`
        - `typescript`
        - `vls`
    - `ruby`
        - `rubocop`
        - `solargraph`
7. Install repos
    - [dockerfiles](https://gitlab.com/jericdeleon/dockerfiles)
    - [notes](https://gitlab.com/jericdeleon/notes)
    - [ledger](https://gitlab.com/jericdeleon/ledger)
    - [password-store](https://gitlab.com/jericdeleon/password-store)
8. Install proprietary codecs (see: [H.264 Video Support](https://en.opensuse.org/SDB:Firefox_MP4/H.264_Video_Support))
    1. Add Packman essentials Repository
        - Tumbleweed:
          ```
          zypper ar -cfp 90 https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/Essentials packman-essentials
          ```
    2. Switch system packages to those in packman:
        ```
        zypper dup --from packman-essentials --allow-vendor-change
        ```
9. Install KVM tools
    1. Add in YAST: Base Technologies - KVM Virtualization Host and Tools
    2. Start / Enable `libvirtd`:
          ```
          sudo systemctl start libvirtd
          sudo systemctl enable libvirtd
          ```
    3. Install `vagrant`:
          ```
          sudo zypper in vagrant
          ```
    4. `firewalld` must be configured for `libvirt` using NFS
      1. List `libvirt` firewalld zone:
          ```
          sudo firewall-cmd --info-zone libvirt
          ```
      3. Add firewalld services to libvirt zone:
          ```
          sudo firewall-cmd --permanent --zone libvirt --add-service nfs
          sudo firewall-cmd --permanent --zone libvirt --add-service nfs3
          sudo firewall-cmd --permanent --zone libvirt --add-service rpc-bind
          sudo firewall-cmd --permanent --zone libvirt --add-service mountd
          sudo firewall-cmd --reload
          ```
      4. To enable NFS v3 and UDP support, create: `/etc/nfs.conf.local`:
          ```
          [nfsd]
          vers3=y
          udp=y
          ```
      5. Test libvirt; error below should not appear:
          ```
          vagrant-libvirt cannot mount to NFS server
          ```

# Notes
1. `git push/pull/clone` is slow
    - SSH supports both IPv4 and IPv6
    - SSH prefers IPv6 if the DNS retrieves AAAA record
    - If AAAA record is obtained but ISP doesn't support IPv6, there will be a delay
    - Fix: Configure `ssh` to prefer IPv4
      1. Add to `~/.ssh/config`:
          ```
          Host *
            AddressFamily inet
          ```
      2. Restart `sshd`: `sudo systemctl restart sshd`

# Resources
- [H.264 Video Support](https://en.opensuse.org/SDB:Firefox_MP4/H.264_Video_Support)
