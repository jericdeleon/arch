# Bootstrap - Arch
Arch installation guide, for the ff. targets:
  - Vagrant (`archlinux/archlinux`)
  - Station

## Notes

### Virtualbox
1. Do not use UEFI
    - Setup will work fine, but booting into secondary media (Arch ISO in CD drive) after Arch install is undoable

2. GPG Guest Lock
    - Guest VM is greedy with the hardware key
        - if key is used once in guest VM, host OS won't be able to access the key
    - Solution: shutdown guest VM to release the key

### ISO
1. Do not upgrade packages while in install media (`-u`)
    - Can cause sync issues

# Installation

## Vagrant
1. Install Vagrant
    1. Install Virtualbox Extension Pack
    2. Add host user to `vboxusers` group (reboot afterwards)

2. Provision Vagrant VM
    ```
    vagrant up
    ```

3. Edit Vagrant VM
    - Enable USB passthrough; hardware key must be detected in guest

4. Start VM, and continue with **Configure User** step in ISO installation

## ISO

1. Flash bootable USB
    ```
    dd bs=4M if=archlinux.iso of=/dev/sdx status=progress oflag=sync
    ```

2. Setup hardware BIOS
    1. Use UEFI boot
    2. Set hardware clock
    3. Enable virtualization

3. Verify boot
    1. Check boot mode
        ```
        ls /sys/firmware/efi/efivars
        ```

    2. Check which drives are TRIM-capable
        ```
        lsblk --discard
        ```

4. Connect to the internet
    - Ethernet:
        ```
        dhcpcd
        ```

    - Wireless:
        ```
        wifi-menu
        ```
5. Open guide
    1. Install editor
        ```
        sudo pacman -S vim
        ```

    2. Load guide
        ```
        curl -sL http://link.jericdeleon.com/arch | vim
        ```

    3. Use `vim` as a multiplexer; guide on one side, terminal in another
        - open terminal: `:term`
        - paste in terminal job mode: `C-w " <register>`
          - `"` is default register

6. Update mirrors
    ```
    pacman -S pacman-contrib
    curl -sL http://link.jericdeleon.com/script-update-mirrors | bash
    ```

7. Update system clock
    ```
    timedatectl set-ntp true
    ```

8. Setup partitions
    - BIOS: `/boot` partition needed for LUKS

        | Mount Point | Type  | Code | Size |
        |-------------|-------|------|------|
        | /mnt/boot   | Linux | 20   | 512M |
        | /mnt        | Linux | 20   | all  |

    - UEFI: EFI partition can be used as `/boot`

        | Mount Point | Type  | Code | Size |
        |-------------|-------|------|------|
        | /mnt/boot   | EFI   |  1   | 512M |
        | /mnt        | Linux | 20   | all  |

    ```
    fdisk -l # list devices (also: lsblk)
    fdisk /dev/sda # partition

    m # help
    p # print table
    n # new partition
    t # change partition type
    v # verify table
    w # write to disk
    q # quit

    # BIOS
    o # create DOS partition table

    # UEFI
    g # create GPT partition table
    ```

9. Setup LUKS
    1. Encrypt partition
        ```
        cryptsetup luksFormat /dev/sda2
        ```

    2. Open crypt
        ```
        cryptsetup open /dev/sda2 cryptlvm
        ```

10. Setup LVM
    1. Create physical volume
        ```
        pvcreate /dev/mapper/cryptlvm
        ```

    2. Create volume group
        ```
        vgcreate cryptvg /dev/mapper/cryptlvm
        ```

    3. Create logical volumes
        ```
        lvcreate -L 1G cryptvg -n swap
        lvcreate -l 100%FREE cryptvg -n root
        ```

11. Format storage

    1. Format root / swap
        ```
        mkswap /dev/cryptvg/swap
        mkfs.ext4 /dev/cryptvg/root
        ```

        - SD cards: disable journaling

              ```
              tune2fs -O "^has_journal" /dev/cryptvg/root
              ```

    2. Format EFI
        - BIOS
            ```
            mkfs.ext4 /dev/sda1
            ```

        - UEFI
            ```
            mkfs.fat -F32 /dev/sda1
            ```

12. Mount partitions
      ```
      swapon /dev/cryptvg/swap
      mount /dev/cryptvg/root /mnt

      mkdir /mnt/boot
      mount /dev/sda1 /mnt/boot
      ```

13. Install essential packages
    1. Gather all packages
        - base: `base linux linux-firmware`
        - kernels:
          - `linux`: better for new hardware
          - `linux-lts`: better for old / constrained hardware
              - Intel Compute Stick (2GB RAM)
          - `linux-zen`: slightly better than `linux` (evaluate in a test run)
        - storage: `lvm2`
        - networking:
          - `NetworkManager`: `networkmanager`
          - `netctl: ``netctl wpa_supplicant dhcpcd dialog ppp`
        - editors: `vim`
        - firmware (check hardware)

    2. Bootstrap
        ```
        pacstrap /mnt base linux linux-firmare ...
        ```

14. Generate `fstab` file
    ```
    genfstab -U /mnt >> /mnt/etc/fstab
    ```

15. `chroot` into system
    ```
    arch-chroot /mnt
    ```

16. Configure timezone
    ```
    ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
    hwclock --systohc --utc
    ```

17. Configure locale
    ```
    curl -sL http://link.jericdeleon.com/arch-provision-locale | bash
    ```

18. Configure host
    ```
    curl -sL http://link.jericdeleon.com/arch-provision-host | bash -s hostname
    ```

19. Configure initramfs
    1. Modify `/etc/mkinitcpio.conf`
        ```
        HOOKS=(base systemd keyboard sd-vconsole autodetect modconf block sd-encrypt sd-lvm2 filesystems fsck)
        ```

    2. Create deps
        ```
        touch /etc/vconsole.conf
        ```

    3. Recreate initramfs
        ```
        mkinitcpio -P
        ```

20. Set root password
    ```
    passwd
    ```

21. Configure microcode updates
    - Intel:
        ```
        pacman -S intel-ucode
        ```

    - AMD:
        ```
        pacman -S amd-ucode
        ```

22. Configure bootloader
    - `systemd-boot`
        1. Install bootloader
            ```
            bootctl --path=/boot install
            ```

        2. Exit `chroot`
        3. Populate bootloader entry conf with device values
            ```
            lsblk -no PATH,UUID /dev/sda2 > /mnt/boot/loader/entries/arch.conf
            /mnt/boot/init* /mnt/boot/vmlinuz* >> /mnt/boot/loader/entries/arch.conf
            ```

        4. Modify bootloader entry conf until it looks like the ff.:
            ```
            title   Arch
            linux   /vmlinuz-linux
            initrd  /intel-ucode.img
            initrd  /initramfs.img
            options rd.luks.name=DEVICE-UUID=cryptlvm root=/dev/cryptvg/root
            ```

        5. Set default bootloader entry in `/mnt/boot/loader/loader.conf`
            ```
            default arch
            timeout 10
            ```

        6. Setup automatic updates by creating `/mnt/etc/pacman.d/hooks/100-systemd-boot.hook`
            ```
            [Trigger]
            Type = Package
            Operation = Upgrade
            Target = systemd

            [Action]
            Description = Updating systemd-boot
            When = PostTransaction
            Exec = /usr/bin/bootctl update
            ```

        7. `chroot` back into the system

    - GRUB
        1. Install packages
            - BIOS
                ```
                pacman -S grub os-prober
                ```

            - UEFI
                ```
                pacman -S grub os-prober efibootmgr
                ```
        2. Modify `/etc/default/grub`
            ```
            GRUB_CMDLINE_LINUX="cryptdevice=/dev/sda2:cryptlvm"
            ```

        3. Install GRUB
            - BIOS
                ```
                grub-install --target=i386-pc /dev/sda --recheck
                ```

            - UEFI
                ```
                grub-install --target=x86_64-efi --efi-directory=/boot --recheck
                ```

        4. Generate GRUB config

            ```
            grub-mkconfig -o /boot/grub/grub.cfg
            ```

23. Provision
    ```
    curl -sL http://link.jericdeleon.com/arch-provision-base | bash
    ```

24. Provision user

    1. Create user

        ```
        pacman -s sudo
        groupadd jericdeleon
        useradd -m -g jericdeleon -G wheel -s $(which zsh) jericdeleon
        passwd jericdeleon
        ```

    2. Uncomment wheel in `sudoers`

        ```
        EDITOR=vim visudo
        ```

    3. Add additional user provisions

        ```
        curl -sL http://link.jericdeleon.com/arch-provision-user | bash -s jericdeleon
        ```

25. Configure user
    1. Login as user (generate i3 default)
    2. Setup network (choose 1)
        - NetworkManager
          ```
          sudo systemctl enable NetworkManager
          sudo systemctl start NetworkManager
          sudo systemctl status NetworkManager
          ```

        - `netctl`
          - Ethernet

              ```
              sudo systemctl enable dhcpcd
              sudo systemctl start dhcpcd
              sudo systemctl status dhcpcd
              ```

          - Wireless

              ```
              wifi-menu
              ```

    3. Insert hardware key
        - Check if detected (must pass to proceed)

            ```
            gpg --card-status
            ```

    4. Configure user
        1. Set `.pam_environment`
            ```
            curl -sLo ~/.pam_environment http://link.jericdeleon.com/dot-pam-environment
            ```

        2. Logout and log back in with a new session, and check if ssh identity is detected
            ```
            ssh-add -L
            ```

        3. Configure
            ```
            curl -sL http://link.jericdeleon.com/dot-setup-gpg | bash
            gpg-connect-agent updatestartuptty /bye
            curl -sL http://link.jericdeleon.com/arch-configure-user | bash
            ```

    5. Setup PAM
        1. Grant execute to scripts

            ```
            jsg
            ```

        2. Associate hardware keys with account

            - First key: `create_yubikey_mapping`
            - Additional keys: `add_yubikey_mapping`

        3. Add `pam_u2f.so` module in `/etc/pam.d/system-auth`
            ```
            ...
            auth required pam_unix.so ...
            auth required pam_u2f.so
            ...
            ```

26. Extras
    1. SSDs
        1. Setup Periodic TRIM

            ```
            sudo systemctl enable fstrim.timer
            ```

    2. Graphics Drivers / Hardware Acceleration
        1. Install drivers
          - Intel
              ```
              sudo pacman -S xf86-video-intel                                  # OpenGL
              sudo pacman -S vulkan-intel                                      # Vulkan
              sudo pacman -S intel-media-driver libva-intel-driver libva-utils # VAAPI
              ```

          - AMD
              ```
              sudo pacman -S xf86-video-amd                # OpenGL
              sudo pacman -S vulkan-radeon amdvlk          # Vulkan
              sudo pacman -S libva-mesa-driver libva-utils # VAAPI
              ```

        2. Check if enabled
            ```
            glxinfo | grep rendering    # OpenGL; must be yes
            ls /usr/share/vulkan/icd.d/ # Vulkan
            vainfo                      # VAAPI
            ```

    3. Sensors
        1. Setup `lm_sensors`

            ```
            sudo sensors-detect
            ```

    4. Power Management
        1. Setup `tlp`
            ```
            sudo pacman -S tlp
            sudo systemctl enable tlp
            sudo systemctl start tlp
            sudo systemctl status tlp
            ```

        2. Setup `tlp-rdw`
            ```
            sudo pacman -S tlp-rdw
            sudo systemctl enable NetworkManager-dispatcher
            sudo systemctl mask systemd-rfkill.service
            sudo systemctl mask systemd-rfkill.socket
            ```

        3. Thinkpad Setup:
            ```
            sudo pacman -S acpi_call
            ```

    5. Networks
        1. Setup network manager
          - NetworkManager

              ```
              nmcli device wifi list                               # list devices
              nmcli device wifi connect <ssid> password <password> # connect to SSID

              nmcli connection                                     # list connections
              nmcli connection delete <ssid>                       # delete connection

              nmcli radio wifi off                                 # turn off wifi
              nmcli radio wifi on                                  # turn off wifi
              ```

          - `netctl`

              ```
              netctl list
              netctl status <profile>
              netctl start <profile>
              netctl enable <profile>
              netctl is-enabled <profile>
              ```

        2. Review `ip` in `i3blocks`

    6. Bluetooth
        1. Check `btusb` kernel module if loaded
            ```
            lsmod | grep btusb
            ```

        2. Setup `bluetooth`
            ```
            sudo systemctl enable bluetooth
            sudo systemctl start bluetooth
            sudo systemctl status bluetooth
            ```

        3. `bluetoothctl`

            ```
            power on      # bluetooth is always off at boot, unless configured
            agent         # select KeyboardOnly agent
            default-agent # set as default
            scan on
            devices
            pair <id>
            trust <id>
            connect <id>
            ```

    7. [Firefox](https://wiki.archlinux.org/index.php/Firefox/Tweaks)
        1. Configure
            - Install Addon: password manager
            - Install Theme: Humble Gruvbox
            - Organize Toolbar

        2. Enable Acceleration (consumes more CPU)
            - Edit flags in `about:config`
              - OpenGL: `layers.acceleration.force-enabled`
              - WebRender: `gfx.webrender.all`
              - Accelerated Azure Canvas: `gfx.canvas.azure.accelerated` (set to `true`)
            - Restart Firefox and check in `about:support`

        3. Setup Tridactyl

            ```
            set theme quake
            set newtabfocus page
            set smoothscroll true
            ```

    8. Hardware-specific
        - [Thinkpad T450s](https://wiki.archlinux.org/index.php/Lenovo_ThinkPad_T450s)
            1. Handle system events
                - https://www.reddit.com/r/i3wm/comments/5g86f1/suspend_on_lid_close/

            2. Setup touchpad and fingerprint
                ```
                sudo pacman -S fprintd
                ```
